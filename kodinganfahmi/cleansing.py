import re
import string
import operator
import random
 
def opentxt(filename):
	with open(filename) as data_file:
		data = data_file.readlines()
	return data

def writefile(array, filename,tipe):
    print len(array)
    file = open(filename,tipe)
    for a in range(len(array)):
        file.write(str(array[a].strip()) + '\n')
    file.close()

def terbilang(bil):
    angka=["","satu","dua","tiga","empat","lima","enam","tujuh","delapan","sembilan","sepuluh","sebelas"]
    Hasil =""
    n = int(bil)
    if n >= 0 and n <= 11:
        Hasil = Hasil + angka[n]
    elif n < 20:
        Hasil = terbilang(n % 10) + " belas"
    elif n < 100:
        Hasil = terbilang(n / 10) + " puluh " + terbilang(n % 10)
    elif n < 200:
        Hasil = "seratus " + terbilang(n - 100)
    elif n < 1000:
        Hasil = terbilang(n / 100) + " ratus " + terbilang(n % 100)
    elif n >= 1000 and n <= 1999 :
        Hasil = "seribu " + terbilang(n % 1000)
    elif n >= 2000 and n <= 999999:
        Hasil= terbilang(n / 1000) + " ribu " + terbilang(n % 1000)
    return Hasil

def clean(data):
    m = re.split('\s*', data.strip("\n"))
    # print data.strip("\n")
    kal = ""
    for word in m:
        mat = re.search("[0-9]",word)
        if mat:
            try:
                word = terbilang(int(word))
            except:
                kal = None
                break
        kal += word + " "
    return kal


def getclean(data):
    sen = []
    for x in range(len(data)):
        res= clean(data[x])
        if res != None:
            sen.append(res)
    sent = list(set(sen))
    sent.sort()
    return sent
    
    
def cleandatasentence(data):
    allsen = []
    for d in data:
        splitdata = re.split('\s*', d.strip("\n"))
        split2 = []
        for word in splitdata:
            if len(word)==1:
                findpunc = re.search("[,.?!]",word) 
                if findpunc and split2 != []:
                    split2[-1] += word
            elif len(word) != 0:
                split2.append(word)
        split3 = []
        for word in split2:
            findpunc = re.search("[,.?!]",word)
            if findpunc and ".co" not in word:
                kal = ""
                for c in word[:-1]:
                    if re.match('[,.?!]', c):
                        kal += c + " "
                    else:
                        kal += c
                kal += word[-1]
                split3.append(kal)
                    
            else:
                split3.append(word)
        allsen.append(" ".join(split3))
    allsen.sort()
    return allsen

def separatesen(array):
    sen =""
    sentences =[]
    temp = []
    countword = 4
    
    for i in range(0,9,countword):
        if len(array[i+countword : i+2*countword]) == 1:
            temp.append(array[i:])
            break
        else:
            temp.append(array[i : i+countword])
        
    for t in temp:
        for word in t:
            sen += word
            if word != t[-1]:
                sen += " "
        punc = re.search("[,.?!]",sen[-1]) 
        if not punc:
            sen += "."
        sentences.append(sen)
        sen =""
        
    return sentences
        
    
        
    
def splitsentence(data):
    sen = []
    for d in data:
        splitdata = re.split('\s*', d.strip("\n"))
        if len(splitdata) > 10:
            sepsen = separatesen(splitdata)
            for s in sepsen:sen.append(s)
        else:
            sen.append(d)
    sen.sort()
    return sen
    
        

def cleanminword(data):
    diction = {}
    dicti={}
    

    #print "get Dictionary"
    for kalimat in range(len(data)):
        splitdata = re.split('\s*', data[kalimat].strip("\n"))
        for word in splitdata:
            word = word.translate(string.maketrans("",""), string.punctuation)
            diction[word.lower()] = diction.get(word.lower(), 0) +1
            dicti.setdefault(word.lower(),[]).append(kalimat)
    
    print "get dictionary"
    worstword =[]
    # sorted_x = sorted(diction.items(), key=operator.itemgetter(1))
    
    for d in diction.keys():
        if "www" in d:
            print d
            worstword += dicti[d]
        
        
    '''   
    # clean worst dictionary
    for d in diction.keys(): 
        if diction[d] < 15:
            #print str(d) + " " + str(diction[d])
            worstword += dicti[d]

    stopword = clearenter(opentxt("stop-word-list.txt"))
    for d in diction.keys():
        if d in stopword:
            worstword += dicti[d]
    '''
        
    print "delete word"
    uniqworst = set(worstword)
    #sortword = sorted(uniqworst, reverse=True)
    print len(uniqworst)
    
    itdata = set([x for x in range(len(data))])
    fixsen = list(itdata - uniqworst)
    
    result = [data[x] for x in fixsen]
    result.sort()
    return result
    
    
def cleanchar(data):
    result = []
    for kalimat in data:
        splitdata = re.split('\s*', kalimat.strip("\n"))
        word = splitdata[-1].translate(string.maketrans("",""), string.punctuation)
        avoidchar = [s for s in splitdata if len(s) > 1]
        if len(avoidchar) > 2 and len(word) > 1:
            result.append(" ".join(avoidchar))
    return result 

def clearenter(data):
    result = [k.strip("\n") for k in data]
    return result
        
def getword(data):
    diction = {}
    for kalimat in data:
        kalimat = kalimat.translate(string.maketrans("",""), string.punctuation)
        splitdata = re.split('\s*', kalimat.strip("\n"))
        for s in splitdata:
            diction[s.lower()] = diction.get(s.lower, 0) +1
                
    return diction.keys()

def getupperfirst(data):
    allsen = []
    for d in data:
        allsen.append(d[0].upper()+d[1:])
    allsen.sort()
    return allsen
        
def app():
    print "hai"
    #gabungan = opentxt("hasilGabungan.txt")
    #cleandatasentence(gabungan)
    
    #cleansent = opentxt("cleansingsentence.txt")
    #splitsentence(cleansent)
    
    # cleansepa = opentxt("separate.txt")
    # getclean(cleansepa)
    
    #cleannum = opentxt("cleansingnumber.txt")
    #cleanminword(cleannum)
    #gabungan = opentxt("hasilGabungan.txt") 

    clean1= opentxt("dataclean.txt")

    # clean2 = cleanchar(clean1)
    # clean3 = cleanminword(clean2)
    # writefile(clean3, "dataclean.txt", "a")
    words = getword(clean1)
    words.sort()
    writefile(words, "kataindo.txt", "a")
    
    
    
#app()

gabungan = opentxt("semogaSidangJuni.txt")
clearsentence = cleandatasentence(gabungan)
writefile(clearsentence, "clearsentence.txt", "a")

separatesen = splitsentence(clearsentence)
writefile(separatesen,"cleanseparate.txt","a")


cleannumber = getclean(separatesen)
writefile(cleannumber ,"cleansingnumber.txt","a")

'''
cleannum = opentxt("cleansingnumber.txt")

data = opentxt("dataclean.txt")
mother = opentxt("MotherSentenceSet.txt")

gabung = list(set(data+mother))
random.shuffle(gabung)
print "hasil gabungan"
baru = gabung
baru.sort()
kata = getword(baru)
kata.sort()
print "hasil kata"
writefile(baru,"dataset.txt","a")
writefile(kata,"kataindo.txt","a")
'''






