import json
import re
import time
import sys

def time(i,lene):
	bar_length = 20
	percent = float(i)/lene
	hashes = '#' * int(round(percent * bar_length))
	spaces = ' ' * (bar_length - len(hashes))
	sys.stdout.write("\rPercent: [{0}] {1}%".format(hashes + spaces, int(round(percent * 100))))
	sys.stdout.flush()

def openjson(filename): 
	with open(filename) as data_file:
		data = json.load(data_file)
	return data

def writefile(array, filename,tipe):
	file = open(filename,tipe)
	for a in array:
		file.write(a.encode('utf-8').strip() + '\n')
	file.close()

def terbilang(bil):
    angka=["","Satu","Dua","Tiga","Empat","Lima","Enam","Tujuh","Delapan","Sembilan","Sepuluh","Sebelas"]
    Hasil =""
    n = int(bil)
    if n >= 0 and n <= 11:
        Hasil = Hasil + angka[n]
    elif n < 20:
        Hasil = terbilang(n % 10) + " Belas"
    elif n < 100:
        Hasil = terbilang(n / 10) + " Puluh " + terbilang(n % 10)
    elif n < 200:
        Hasil = "Seratus " + terbilang(n - 100)
    elif n < 1000:
        Hasil = terbilang(n / 100) + " Ratus " + terbilang(n % 100)
    elif n >= 1000 and n <= 1999 :
        Hasil = "Seribu " + terbilang(n % 1000)
    elif n >= 2000 and n <= 999999:
        Hasil= terbilang(n / 1000) + " Ribu " + terbilang(n % 1000)
    return Hasil

def splittext(text):
	m = re.split('\s*', text)
	sen = ""
	kal = []
	for word in m:
		s = re.sub(r'^"|"$', '', word)											# subtitute quote
		if len(s) != 0:
			sama = re.search('[?!.,]',s[-1])									# find punctuation last string
			if sama:
				if sen == ""  or re.match('[?!.,]',s):
					sen += s + " "
				else:
					sen += s
					cor = re.sub(r'\s([?.!,"](?:\s|$))', r'\1', sen)			# remove space in before punctuation
					al = re.search('[a-zA-Z0-9]',cor)									# find alphebet 
					if al:
						kal.append(cor)
						sen = ""

			else:
				sen += s + " "

	return kal

def splittext2(text):
	sen =""
	kal=[]
	text = re.sub(r'\s([?.!,"](?:\s|$))', r'\1', text)
	mspltxt = re.split('\s*', text)
	for wd in mspltxt:
		wd = re.sub(r'[\'\"\xc2\xa3:*]',"",wd)
		sama = re.search('[?!.,]',wd[-1])
		if sama:
			if sen == "" :
				sen += wd + " "
			else:
				sen += wd
				sen = " ".join(re.split("[^a-zA-Z0-9,.?!]*", sen))

				kal.append(sen)
				sen=""
		else:
			sen += wd + " "
	return kal

		



# cakupan cleansing
# urutan tidak termasuk
# angka hanya 3 digit
# tanggal tidak termasuk
# simbol
 
i = 0
f=0
# data =openjson("../datacrawl/vivanews.json")
data=openjson("viva.json")

for d in data:
	for dber in d['berita']:
		teskalimat=  dber.encode('utf-8')
		res = splittext2(teskalimat)
 		if i%1000000==0:
 			f +=1
 			writefile(res,"1jtke-"+str(f)+".txt","w")
 		else:
 			writefile(res,"1jtke-"+str(f)+".txt","a")
 		for r in res:
 			i+=1

print i 


# teskalimat =  data[0]['berita'][1]
# kal = splittext(teskalimat)
# print kal

